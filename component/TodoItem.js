import html from '../core.js'

function TodoItem({todo, index, editIndex}) {
    // console.log(todo)
    if (editIndex === index) {
        setTimeout(() => {
            const editInput = document.querySelector('.editing .edit');
            if (editInput) {
                editInput.focus();
                editInput.setSelectionRange(editInput.value.length, editInput.value.length);
            }
        }, 0);
    }
    return html`
    <li class="${todo.completed && 'completed'} ${editIndex===index && 'editing'}">
        <div class="view">
            <input class="toggle" 
            type="checkbox" ${todo.completed && 'checked'}
            onchange="dispatch('toggle', ${index})"
            >

            <label ondblclick= "dispatch('startEdit',${index})">${todo.title}</label>
            <button class="destroy" onclick="dispatch('destroy', ${index})" ></button>
        </div>
        <input class="edit" value="${todo.title}"
            onkeyup="event.keyCode === 13 && dispatch('updateEditEnd', this.value.trim()) 
            ||event.keyCode === 27 && dispatch('cancelEditEnd')"
            onblur="dispatch('updateEditEnd', this.value.trim())"
           
        >
    </li>
    `
}

export default TodoItem
//connect  de lay editIndex