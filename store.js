import logger from "./component/logger.js";
import { createStore } from "./core.js";
import reducer from "./reducer.js";

const {attach, connect, dispatch} = createStore(logger(reducer))

window.dispatch = dispatch

export {
    attach,
    connect
}